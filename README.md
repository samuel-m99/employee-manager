**!This is proof of concept only. Data doesn't save.**

The Employee Manager is a practical and user-friendly application developed using React, CSS, HTML, and Bootstrap elements. When deciding on a project, I wanted to build something that could actually be used in the real world.

<h2>How to Use:<h2>

- Fill the input fields with your desired information (name, surname, position, team) and click "Pridať" (Add).
- If you want to delete an employee, click on "Zmazať" (Delete) next to the corresponding worker.
- To edit a worker, fill the input fields with your updated information, then click the "Zmeniť" (Edit) button next to the corresponding worker.