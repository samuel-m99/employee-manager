import React from 'react';

export default function InputField(props) {
    	const handleChange = (event) => {
        props.setValue(event.target.value);
    };

    return (
            <div className="input-field">
                <label>{props.label}:
                    <br></br>
                    <input type="text" value={props.value} onChange={handleChange}/>
                </label>
            </div>
    )
}
