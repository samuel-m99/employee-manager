import React from 'react';

let nextId = 0;

export default function AddWorker(props) {

    const onClickHandler = () => {
        props.setWorkers([
            ...props.workers,
            { id: nextId++, firstName: props.firstName , lastName: props.lastName , position: props.position , team: props.team}
        ]);
        clearInputFields();
    };

    const clearInputFields = () => {
        props.setFirstName('');
        props.setLastName('');
        props.setPosition('');
        props.setTeam('');
    };

    return (
        <div className="d-grid gap-2 d-md-flex justify-content-md-center">
            <button className="btn btn-success" style={{marginBottom: 20}} onClick={onClickHandler}>Pridať</button>
        </div>
    )
}
