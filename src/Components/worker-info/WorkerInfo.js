import React from 'react';

export default function WorkerInfo(props) {

    return (
        <div>
            <p>Celé meno: {props.fullName}</p>
            <p>Pozícia: {props.position}</p>
            <p>Team: {props.team}</p>
        </div>
    )
}
